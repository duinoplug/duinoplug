#ifndef FORMATS_H
#define FORMATS_H

typedef enum _format {
	JSON,
	PLAIN
} Format;

String floatToString(float f) {
	int a, b;
	char out[6];

	a = (int)f;
	b = (int)(f*100);

	sprintf(out, "%02d.%02d", a, b);

	return String(out);
}

#endif // FORMATS_H
