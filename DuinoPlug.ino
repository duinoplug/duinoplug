#include <SPI.h>
#include <Ethernet.h>

// For LCD 16x2 HD44780 Screen with I2C connection
#include <Wire.h>
#include <hd44780.h>
#include <hd44780ioClass/hd44780_I2Cexp.h> // include i/o class header
hd44780_I2Cexp lcd;

#define BOARDNAME "DuinoPlug"
#define BOARDVERSION "2.0"

#define ETHERNET_DHCP
// #define ETHERNET_STATIC

#define SERIAL_DEBUG 1
// #define WEBDUINO_SERIAL_DEBUGGING 1

// #define DUINOPLUG_DACREF_INTERNAL_2v56
#define DUINOPLUG_DACREF_INTERNAL_VCC

/**
   Frequency of the current to measure
   Default: 50Hz (Europe)
 **/
#define DUINOPLUG_FREQUENCY 50

/**
   How long does it takes to analogRead()
   Default: 100 microseconds
   cf http://www.arduino.cc/en/Reference/AnalogRead
*/
#define DUINOPLUG_READTIME 0.00001

/**
   How much samples we should read before being
   sure to get the max voltage
   Default: 2*(number of read to cover one period)
 **/
#define DUINOPLUG_SAMPLES_NUMBER 0.2*((1.0/DUINOPLUG_FREQUENCY)/DUINOPLUG_READTIME)

/**
   Defining maximum value of the DAC
 **/
#define DUINOPLUG_DACREF_MAXVALUE 1023.0

/**
   Defines how much volt for one amp
   specs of hardware (Talema AC1005) gives 0.1v for 1A
 **/
#define DUINOPLUG_VOLTAGE_CURRENT_RATIO 0.1

/**
   DAC reference voltage
 **/
#ifdef DUINOPLUG_DACREF_INTERNAL_2v56
#define DUINOPLUG_DACREF_VOLTAGE 2.56
#endif

/**
   Should be 5.00 but when using USB and measuring,
   getting 4.68v
 **/
#ifdef DUINOPLUG_DACREF_INTERNAL_VCC
#define DUINOPLUG_DACREF_VOLTAGE 5.00
#endif

/**
   2.5v on 5v => 1023.0/(5.0/2.5) == 511.0
   1.25v on 2.56v => 1023.0/(2.56/1.25)
 **/
#ifdef DUINOPLUG_DACREF_INTERNAL_2v56
#define DUINOPLUG_ADDED_OFFSET 1.25
#endif
#ifdef DUINOPLUG_DACREF_INTERNAL_VCC
#define DUINOPLUG_ADDED_OFFSET 2.5
#endif
#define COURANT_OFFSET (DUINOPLUG_DACREF_MAXVALUE/(DUINOPLUG_DACREF_VOLTAGE/DUINOPLUG_ADDED_OFFSET))

/* 1023 --> 4.68v  donc 0.1v --> 21.85 (règle de 3) */
#define COEF_COURANT ((DUINOPLUG_VOLTAGE_CURRENT_RATIO*DUINOPLUG_DACREF_MAXVALUE)/DUINOPLUG_DACREF_VOLTAGE)

/**
   Efficient value = max value / sqrt(2);
   In our case (internal reference VCC, powered by USB at 4.68v)
   we need to use 1.31 instead of sqrt(2)
 **/
#ifdef DUINOPLUG_DACREF_INTERNAL_2v56
// use sqrt(2)
#define COEF_EFFICACE 1.4142
#endif
#ifdef DUINOPLUG_DACREF_INTERNAL_VCC
#define COEF_EFFICACE 1.31
#endif

#include <WString.h>
#include "WebServer.h"
#include "DuinoPlugBoard.h"

/* Création du serveur WebDuino, sur le port 80 */
#define WEBDUINO_PREFIX ""
WebServer webduino(WEBDUINO_PREFIX, 80);

/* Create the board */
DuinoPlugBoard carte(8);

/* Adresse MAC */
byte mac[] = { 0xDE, 0xAD, 0xBE, 0x00, 0x00, 0x00 };

/* Static IP address */
#ifdef ETHERNET_STATIC
byte ip[] = { 192, 168, 1, 242 };
#endif

/* Handlers HTTP */
void indexCmd(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete) {
#ifdef ETHERNET_DHCP
  const char *hostName = "N/A";
  const IPAddress ipAddr = Ethernet.localIP();
  const IPAddress gatewayAddr = Ethernet.gatewayIP();
  const IPAddress dnsAddr = Ethernet.dnsServerIP();
#endif

  if (type != WebServer::GET) {
    server.httpFail();
  } else {
    server.httpSuccess();
  }

  server.print("<html><head><title>" BOARDNAME "/" BOARDVERSION "</title></head>");
  server.print("<body>");
  server.print("<h1>Welcome!</h1>");
  server.print("<p>This DuinoPlug only serves REST API. Code is available at <a href=\"https://gitlab.com/duinoplug/duinoplug\">GitLab' Page</a></p>");
  server.print("<p>");
#ifdef ETHERNET_DHCP
  server.print("DHCP Informations:<ul>");
  server.print("<li>Board Host name: "); server.print(hostName); server.print("</li>");
  server.print("<li>Board IP address: "); server.print(ipAddr); server.print("</li>");
  server.print("<li>Gateway IP address: "); server.print(gatewayAddr); server.print("</li>");
  server.print("<li>DNS IP address is: "); server.println(dnsAddr); server.print("</li>");
  server.print("</ul></p>");
#endif
  server.print("</body>");
  server.print("</html>");
}

void erreurCmd(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete) {
  server.httpFail();
}

void genericHandler(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete) {
  String url(url_tail);
  bool found = false;

  if (!url.endsWith("/")) {
    url += "/";
    server.httpRedirect(url);
  }

  /* Remove leading '/' */
  url = url.substring(1);

  if (url.startsWith(carte.GetRestObject())) {
    found = true;
    carte.RestHandler(server, type, url);
  }

  if (!found) {
    erreurCmd(server, type, url_tail, tail_complete);
  }
}

#define LcdDisplay(str, col, row) \
  lcd.setCursor(col, row); \
  lcd.print(str); \

#define LcdClearDisplay2(str1, str2) \
  lcd.clear(); \
  LcdDisplay(str1, 0, 0); \
  LcdDisplay(str2, 0, 1); \

void setup() {
  // initialize LCD with number of columns and rows:
  lcd.begin(16, 2);
  LcdClearDisplay2(BOARDNAME "/" BOARDVERSION, "Booting ...");
#ifdef SERIAL_DEBUG
  Serial.begin(9600);
  Serial.println(BOARDNAME "/" BOARDVERSION " initializing ...");
#endif

#ifdef DUINOPLUG_DACREF_INTERNAL_2v56
#ifdef SERIAL_DEBUG
  Serial.println("Setting DAC reference to INTERNAL2V56");
  LcdClearDisplay2("DAC ref", "INTERNAL2V56");
#endif
  analogReference(INTERNAL2V56);
#else
#ifdef SERIAL_DEBUG
  Serial.println("Setting DAC reference to Vcc");
  LcdClearDisplay2("DAC ref", "Vcc");
#endif
  analogReference(DEFAULT);
#endif

#ifdef SERIAL_DEBUG
  Serial.println("Displaying constant values:");
  Serial.print("	DUINOPLUG_SAMPLES_NUMBER: ");
  Serial.println(DUINOPLUG_SAMPLES_NUMBER);
  Serial.print("	DUINOPLUG_DACREF_MAXVALUE: ");
  Serial.println(DUINOPLUG_DACREF_MAXVALUE);
  Serial.print("	DUINOPLUG_VOLTAGE_CURRENT_RATIO: ");
  Serial.println(DUINOPLUG_VOLTAGE_CURRENT_RATIO);
  Serial.print("	DUINOPLUG_DACREF_VOLTAGE: ");
  Serial.println(DUINOPLUG_DACREF_VOLTAGE);
  Serial.print("	DUINOPLUG_ADDED_OFFSET: ");
  Serial.println(DUINOPLUG_ADDED_OFFSET);
  Serial.print("	COURANT_OFFSET: ");
  Serial.println(COURANT_OFFSET);
  Serial.print("	COEF_COURANT: ");
  Serial.println(COEF_COURANT);
  Serial.print("	COEF_EFFICACE: ");
  Serial.println(COEF_EFFICACE);
#endif

#ifdef ETHERNET_DHCP
#ifdef SERIAL_DEBUG
  Serial.println("Starting Ethernet with DHCP.");
  LcdClearDisplay2("Init DHCP", "");
#endif
  Ethernet.begin(mac);

  // Since we're here, it means that we now have a DHCP lease, so we print
  // out some information.
  const IPAddress ipAddr = Ethernet.localIP();
  const IPAddress gatewayAddr = Ethernet.gatewayIP();
  const IPAddress dnsAddr = Ethernet.dnsServerIP();

  Serial.println("A DHCP lease has been obtained.");

  Serial.print("My IP address is ");
  Serial.println(ipAddr);
  LcdClearDisplay2("DHCP:", ipAddr);

  Serial.print("Gateway IP address is ");
  Serial.println(gatewayAddr);

  Serial.print("DNS IP address is ");
  Serial.println(dnsAddr);
#endif
#ifdef ETHERNET_STATIC
#ifdef SERIAL_DEBUG
  Serial.println("Starting Ethernet with static assignment.");
  LcdClearDisplay2("Static:", ip);
#endif
  Ethernet.begin(mac, ip);
#endif

#ifdef SERIAL_DEBUG
  Serial.println("Creating board layout.");
#endif

  /* Counting number of plugs */
  carte.appendPlug(DuinoPlug(2, A8));
  carte.appendPlug(DuinoPlug(3, A9));
  carte.appendPlug(DuinoPlug(4, A10));
  carte.appendPlug(DuinoPlug(5, A11));
  carte.appendPlug(DuinoPlug(6, A12));
  carte.appendPlug(DuinoPlug(7, A13));
  carte.appendPlug(DuinoPlug(8, A14));
  carte.appendPlug(DuinoPlug(9, A15));

#ifdef SERIAL_DEBUG
  Serial.println("Instanciating WebDuino.");
#endif

  /* Démarrage du serveur Web */
  webduino.setDefaultCommand(&indexCmd);
  webduino.setFailureCommand(&genericHandler);
  webduino.begin(); 
}

void loop () {
  char buff[256];
  int len = 256;

  webduino.processConnection(buff, &len);

#ifdef ETHERNET_DHCP
  Ethernet.maintain();
#endif

  unsigned int msgs = carte.getPlugsNr();
  static unsigned int counter = 0;
  static unsigned int period = 0;

  if (period == 0) {
    if (counter == msgs) {
      LcdClearDisplay2("IP Address:", Ethernet.localIP());
      counter = 0;
    } else {
      int plugId = counter;
      String status = (carte.getPlug(counter).getPowerStatus() == false) ? "ON" : "OFF";
      String current = String(carte.getPlug(counter).getCurrentMeasured()) + "A";
      LcdClearDisplay2(String("Plug #" + String(counter) + ":"), status + " -- " + current);
      counter++;
    }
  }

  if (period == 131072) {
    period = 0;
  } else {
    period++;
  }
}
