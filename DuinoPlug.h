#ifndef DUINOPLUG_H
#define DUINOPLUG_H

#include "formats.h"

class DuinoPlug {
private:
	int powerControlPin; /* which pin is used to turn off/on the plug  */
	int currentPin; /* analog pin to use to read current */

	bool powerStatus; /* current status of the plug: false => on, true => off */
	float currentMeasured; /* latest measured value of the current */
	int err;

public:
	enum Method {
		CONTROLPIN,
		CURRENTPIN,
		STATUS,
		CURRENT,
		POWERON,
		POWEROFF,
		SWITCH
	};

	enum ErrorCode {
		ERROR_SUCCESS,
		ERROR_CURRENT_OVERFLOW
	};

	/* setters */
	void setPowerControlPin(int);
	void setCurrentPin(int);
	void setPowerStatus(bool);
	void setCurrentMeasured(float);
	void setError(int);

	/* getters */
	int getPowerControlPin(void);
	int getCurrentPin(void);
	bool getPowerStatus(void);
	float getCurrentMeasured(void);
	int getError(void);

	DuinoPlug(int, int);
	void measureCurrent(void);
	bool getPlugStatus(void);
	void enablePlug(void);
	void disablePlug(void);
	void switchPlug(void);

	void toString(Format, Print &, DuinoPlug::Method);
};

void DuinoPlug::setPowerControlPin(int v) {
	this->powerControlPin = v;
}

void DuinoPlug::setCurrentPin(int v) {
	this->currentPin = v;
}

void DuinoPlug::setPowerStatus(bool v) {
	this->powerStatus = v;
}

void DuinoPlug::setCurrentMeasured(float v) {
	this->currentMeasured = v;
}

void DuinoPlug::setError(int v) {
	this->err = v;
}

int DuinoPlug::getPowerControlPin(void) {
	return this->powerControlPin;
}

int DuinoPlug::getCurrentPin(void) {
	return this->currentPin;
}

/**
 * Get the power status of the plug: 
 *  - false means the PLUG is ON
 *  - true means the PLUG is OFF
 **/
bool DuinoPlug::getPowerStatus(void) {
	return this->powerStatus;
}

/**
 * Get the PLUG status: 
 *  - false means the PLUG is OFF
 *  - true means the PLUG is ON
 * This is a helper function, to ease manipulation
 **/
bool DuinoPlug::getPlugStatus(void) {
	return (!this->powerStatus);
}

float DuinoPlug::getCurrentMeasured(void) {
	return this->currentMeasured;
}

int DuinoPlug::getError(void) {
	return this->err;
}

DuinoPlug::DuinoPlug(int _powerControlPin, int _currentPin) :
	powerControlPin(_powerControlPin),
	currentPin(_currentPin),
	powerStatus(false),
	currentMeasured(0.0)
{
	/* Set pin to OUTPUT mode */
	pinMode(_powerControlPin, OUTPUT);

	/* Enable output by default */
	digitalWrite(_powerControlPin, LOW);

	// this->measureCurrent();
}

void DuinoPlug::measureCurrent(void) {
	float max = 0;
	float cur = 0;

	this->setError(DuinoPlug::ERROR_SUCCESS);

	/* Reading analog values */
	for (int j = 0; j < DUINOPLUG_SAMPLES_NUMBER; j++) {
		cur = analogRead(this->currentPin);
		if (cur > max) {
			max = cur;
		}
	}

	if (max >= DUINOPLUG_DACREF_MAXVALUE) {
		this->setError(DuinoPlug::ERROR_CURRENT_OVERFLOW);
	}

	float realVolt = ((max-COURANT_OFFSET)/COEF_COURANT)/COEF_EFFICACE;

	if (realVolt < 0) {
		realVolt = 0.0;
	}

	return this->setCurrentMeasured(realVolt);
}

/**
 * Disable the plug, put the relay UP
 **/
void DuinoPlug::disablePlug(void) {
	digitalWrite (this->powerControlPin, HIGH);
	this->powerStatus = true;
}

/**
 * Enable the plug, put the relay DOWN
 **/
void DuinoPlug::enablePlug(void) {
	digitalWrite (this->powerControlPin, LOW);
	this->powerStatus = false;
}

/**
 * Switch the plug:
 * - if the current power status is FALSE, the plug is ON so DISABLE it
 * - if the current power status is TRUE, the plug is OFF so ENABLE it
 **/
void DuinoPlug::switchPlug(void) {
	if (!this->powerStatus) {
		this->disablePlug();
	} else {
		this->enablePlug();
	}
}

void DuinoPlug::toString(Format f, Print &s, DuinoPlug::Method m) {
	const char *err = "";
	int ret = 0;
	switch(f) {
		case JSON:
			s.print("{");
			switch(m) {
				/* GET methods */
				case DuinoPlug::CONTROLPIN:
					s.print("\"current\":"); s.print(this->getPowerControlPin()); s.print(",");
				break;
				case DuinoPlug::CURRENTPIN:
					s.print("\"current\":"); s.print(this->getCurrentPin()); s.print(",");
				break;
				case DuinoPlug::STATUS:
					s.print("\"status\":"); s.print(this->getPlugStatus()); s.print(",");
				break;
				case DuinoPlug::CURRENT:
					this->measureCurrent();
					ret = this->getError();
					s.print("\"current\":"); s.print(this->getCurrentMeasured()); s.print(",");
				break;

				/* POST methods */
				case DuinoPlug::POWERON:
					this->enablePlug();
				break;
				case DuinoPlug::POWEROFF:
					this->disablePlug();
				break;
				case DuinoPlug::SWITCH:
					this->switchPlug();
				break;
			}
			s.print("\"errorcode\":"); s.print(ret); s.print(",");
			s.print("\"errormsg\":\""); s.print(err); s.print("\"");
			s.print("}");
		break;

		case PLAIN:
		break;

		default:
		break;
	}
}

#endif // DUINOPLUG_H
