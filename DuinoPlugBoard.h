#ifndef DUINOPLUGBOARD_H
#define DUINOPLUGBOARD_H

#include "DuinoPlug.h"

class DuinoPlugBoard {
private:
	DuinoPlug *plugs;
	int plugsNr;

public:
	DuinoPlugBoard(int);
	void appendPlug(DuinoPlug);
	void measurePlug(int);
	void measurePlugs(void);

	void setPlug(int, DuinoPlug);
	DuinoPlug& getPlug(int);

	int getPlugsNr(void);

	void toString(Format, Print &);
	String GetRestObject(void);
	void RestHandler(WebServer &, WebServer::ConnectionType, String &);
};

DuinoPlugBoard::DuinoPlugBoard(int n) {
	this->plugs = (DuinoPlug *)malloc(sizeof(*plugs)*n);
	if (this->plugs == NULL) {
		return;
	}

	this->plugsNr = 0;
}

void DuinoPlugBoard::appendPlug(DuinoPlug p) {
	this->setPlug(this->getPlugsNr(), p);
}

void DuinoPlugBoard::measurePlug(int pos) {
	if ( (pos <= this->plugsNr) && (pos >= 0) ) {
		this->plugs[pos].measureCurrent();
	}
}

void DuinoPlugBoard::measurePlugs(void) {
	for (int i = 0; i < this->getPlugsNr(); i++) {
		this->plugs[i].measureCurrent();
	}
}

void DuinoPlugBoard::setPlug(int pos, DuinoPlug p) {
	if ( (pos <= this->plugsNr) && (pos >= 0) ) {
		this->plugs[pos] = p;
		this->plugsNr++;
	}
}

DuinoPlug& DuinoPlugBoard::getPlug(int pos) {
	if ( (pos <= this->plugsNr) && (pos >= 0) ) {
		return this->plugs[pos];
	}
}

int DuinoPlugBoard::getPlugsNr(void) {
	return this->plugsNr;
}

void DuinoPlugBoard::toString(Format f, Print &s) {
	switch(f) {
		case JSON:
			s.print("{\"plugs\":[");
			for (int iPrise = 0; iPrise < this->getPlugsNr(); iPrise++) {
				s.print("{\"plug\":");
				s.print(iPrise);
				s.print("}");

				if (iPrise != (this->getPlugsNr() - 1)) {
					s.print(",");
				}
			}
			s.print("]}");
		break;

		case PLAIN:
		break;

		default:
		break;
	}
}

String DuinoPlugBoard::GetRestObject(void) {
	return String("plug/");
}

void DuinoPlugBoard::RestHandler(WebServer &server, WebServer::ConnectionType type, String &url) {
#define successJSON server.httpSuccess("application/json")
	if (url == this->GetRestObject()) {
		successJSON;
		this->toString(JSON, server);
	} else {
		char sprise[3];
		int prise;
		String request_uri = url.substring(this->GetRestObject().length());

		String idPrise = request_uri.substring(0, request_uri.indexOf("/"));
		String method = request_uri.substring(idPrise.length() + 1, request_uri.indexOf("/", idPrise.length() + 1));

		idPrise.toCharArray(sprise, 3);
		prise = atoi(sprise);

		if (prise >= 0 && prise < this->getPlugsNr()) {
			if (type == WebServer::GET) {
				if (method == "controlpin") {
					successJSON;
					this->getPlug(prise).toString(JSON, server, DuinoPlug::CONTROLPIN);
				} else if (method == "currentpin") {
					successJSON;
					this->getPlug(prise).toString(JSON, server, DuinoPlug::CURRENTPIN);
				} else if (method == "status") {
					successJSON;
					this->getPlug(prise).toString(JSON, server, DuinoPlug::STATUS);
				} else if (method == "current") {
					successJSON;
					this->getPlug(prise).toString(JSON, server, DuinoPlug::CURRENT);
				} else {
					server.httpNotImplemented();
				}
			} else if (type == WebServer::POST) {
				if (method == "poweron") {
					successJSON;
					this->getPlug(prise).toString(JSON, server, DuinoPlug::POWERON);
				} else if (method == "poweroff") {
					successJSON;
					this->getPlug(prise).toString(JSON, server, DuinoPlug::POWEROFF);
				} else if (method == "switch") {
					successJSON;
					this->getPlug(prise).toString(JSON, server, DuinoPlug::SWITCH);
				} else {
					server.httpNotImplemented();
				}
			} else {
				server.httpMethodNotAllowed("Allow: GET POST");
			}
		} else {
			server.httpNotFound();
		}
	}

	server.println();
}

#endif // DUINOPLUGBOARD_H
